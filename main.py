gender = input("Enter gender: ")
age = int(input("Enter age in years: "))
weight = int(input("Enter weight in kg: "))
height = int(input("Enter height in cm: "))

if 'm' in gender or 'M' in gender:
    BMR = 88.362 + (13.397 * weight)+(4.799 * height) - (5.677 * age)
    print(f"Your BMR is {BMR} calories/day")
elif 'f' in gender or 'F' in gender:
    BMR = 447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)
    print(f"Your BMR is {BMR} calories/day")
